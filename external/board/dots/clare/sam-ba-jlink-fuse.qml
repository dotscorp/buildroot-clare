import SAMBA 3.2
import SAMBA.Connection.JLink 3.2
import SAMBA.Device.SAMA5D2 3.2

JLinkConnection {

	speed: 5000

	device: SAMA5D2 {
	}

	onConnectionOpened: {
		// initialize boot config applet
		initializeApplet("bootconfig")

		// Enable FUSE_EN
		print("Enabling Fuse supply")
		writeu32(0xFC0380C0,0x10000000)
		writeu32(0xFC0380C4,0x0100)
		writeu32(0xFC0380D0,0x10000000)
		Utils.msleep(100)
		
		print("Programming Fuse register")
		// Disable BUREG
		applet.writeBootCfg(BootCfg.BSCR, 0x00)
		// Program Fuse
		applet.writeBootCfg(BootCfg.FUSE, 0x00040DFF)
		
		printBootConfig()
	}

	// read and display current BSCR/BUREG/FUSE values
	function printBootConfig() {
		var bscr = applet.readBootCfg(BootCfg.BSCR)
		print("BSCR=" + Utils.hex(bscr, 8) + " / " + BSCR.toText(bscr))
		var bureg0 = applet.readBootCfg(BootCfg.BUREG0)
		print("BUREG0=" + Utils.hex(bureg0, 8) + " / " + BCW.toText(bureg0))

		var fuse = applet.readBootCfg(BootCfg.FUSE)
		print("FUSE=" + Utils.hex(fuse, 8) + " / " + BCW.toText(fuse))
	}
}
