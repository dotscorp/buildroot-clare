#!/bin/sh

image_dir="/dots/boot"
running_img_name="linux.itb"
update_img_name="linux.itb.update"

# Extract file
xz -cd $1 > $image_dir/$update_img_name
if [ $? -ne 0 ]; then
	exit 1
fi

# Verify
fit_check_sign -f $image_dir/$update_img_name -k /etc/public_key.dtb
if [ $? -ne 0 ]; then
	exit 2
fi

# Overwrite running image
mv $image_dir/$update_img_name  $image_dir/$running_img_name
if [ $? -ne 0 ]; then
	exit 3
fi

# Reboot
reboot
