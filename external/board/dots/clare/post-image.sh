#!/bin/sh

# Create u-boot.cip
UBOOT_KEY_FILE=tmp
if [ -f $BINARIES_DIR/u-boot.cip ]; then
    rm $BINARIES_DIR/u-boot.cip
fi

until pkcs11-tool --module opensc-pkcs11.so --login --read-object --type data --label uboot_key --output-file $UBOOT_KEY_FILE; do
    echo -n "pkcs11-tool failed. Did you connect the NitroKey HSM dongle? Press Enter to retry."
    read null
done
~/Programs/secure-sam-ba-cipher-3.2.2/secure-sam-ba-cipher.py application -l ~/Programs/secure-sam-ba-cipher-3.2.2/sama5d2x_lic.txt -k $UBOOT_KEY_FILE -d sama5d2 -i $BINARIES_DIR/u-boot.bin  -o $BINARIES_DIR/u-boot.cip || exit 1
rm -f $UBOOT_KEY_FILE

# Make FIT image
UBIFS=$BINARIES_DIR/ubifs
ITB_DIR=$UBIFS/boot
mkdir -p $ITB_DIR
cp $BR2_EXTERNAL_MCHP_PATH/board/dots/clare/linux.its $BINARIES_DIR/
mkimage -f $BINARIES_DIR/linux.its $ITB_DIR/linux.itb
if [ $? -ne 0 ]; then
    echo "Fit image creation failed"
    exit 3
fi

#
# Sign image
#
until mkimage -F -k $(p11tool --list-tokens | sed -n -e 's/.*model=PKCS/model=PKCS/p') -N pkcs11 -r $ITB_DIR/linux.itb; do
    echo -n "Image signing failed. Did you connect the NitroKey HSM dongle? Press Enter to retry."
    read null
done

# Make UBIFS
UBIFS_OPTS="-e 0x1f000 -c 1024 -m 0x800 -x none"
mkfs.ubifs -r $UBIFS $UBIFS_OPTS -o $BINARIES_DIR/dots.ubifs
if [ $? -ne 0 ]; then
    echo "UBIFS creation failed"
    exit 5
fi

#
# Make UBI device
#
UBINIZE_OPTS="-m 0x800 -p 0x20000 -s 2048"

# Make ubinize config
cat <<EOT >> $BINARIES_DIR/ubinize.conf
[dots]
mode=ubi
vol_id=0
vol_type=dynamic
vol_name=dots
vol_alignment=1
vol_flags=autoresize
image=$BINARIES_DIR/dots.ubifs
EOT

ubinize -o $BINARIES_DIR/dots.ubi $UBINIZE_OPTS $BINARIES_DIR/ubinize.conf
if [ $? -ne 0 ]; then
    echo "UBI creation failed"
    exit 6
fi

#
# Clean
#
rm $BINARIES_DIR/ubinize.conf




