#!/bin/sh

#
# Fill u-boot key
#
UBOOT_KEY_FILE=$1/tmp

until pkcs11-tool --module opensc-pkcs11.so --login --read-object --type data --label uboot_key --output-file $UBOOT_KEY_FILE; do
    echo -n "pkcs11-tool failed. Did you connect the NitroKey HSM dongle? Press Enter to retry."
    read null
done

UBOOT_KEY=`sed -n -e s/^KEY=//p $UBOOT_KEY_FILE`
UBOOT_IV_KEY=`sed -n -e s/^IV_KEY=//p $UBOOT_KEY_FILE`
UBOOT_CMAC_KEY=`sed -n -e s/^CMAC_KEY=//p $UBOOT_KEY_FILE`
rm -f $UBOOT_KEY_FILE


sed -i 's/\(^CONFIG_AES_CIPHER_KEY_WORD0=\).*/\10x'`echo $UBOOT_KEY | cut -c 1-8`/   $1/.config || exit 1
sed -i 's/\(^CONFIG_AES_CIPHER_KEY_WORD1=\).*/\10x'`echo $UBOOT_KEY | cut -c 9-16`/  $1/.config || exit 2
sed -i 's/\(^CONFIG_AES_CIPHER_KEY_WORD2=\).*/\10x'`echo $UBOOT_KEY | cut -c 17-24`/ $1/.config || exit 3
sed -i 's/\(^CONFIG_AES_CIPHER_KEY_WORD3=\).*/\10x'`echo $UBOOT_KEY | cut -c 25-32`/ $1/.config || exit 4
sed -i 's/\(^CONFIG_AES_CIPHER_KEY_WORD4=\).*/\10x'`echo $UBOOT_KEY | cut -c 33-40`/ $1/.config || exit 5
sed -i 's/\(^CONFIG_AES_CIPHER_KEY_WORD5=\).*/\10x'`echo $UBOOT_KEY | cut -c 41-48`/ $1/.config || exit 6
sed -i 's/\(^CONFIG_AES_CIPHER_KEY_WORD6=\).*/\10x'`echo $UBOOT_KEY | cut -c 49-56`/ $1/.config || exit 7
sed -i 's/\(^CONFIG_AES_CIPHER_KEY_WORD7=\).*/\10x'`echo $UBOOT_KEY | cut -c 57-64`/ $1/.config || exit 8
sed -i 's/\(^CONFIG_AES_IV_WORD0=\).*/\10x'`echo $UBOOT_IV_KEY | cut -c 1-8`/   $1/.config || exit 9
sed -i 's/\(^CONFIG_AES_IV_WORD1=\).*/\10x'`echo $UBOOT_IV_KEY | cut -c 9-16`/  $1/.config || exit 10
sed -i 's/\(^CONFIG_AES_IV_WORD2=\).*/\10x'`echo $UBOOT_IV_KEY | cut -c 17-24`/ $1/.config || exit 11
sed -i 's/\(^CONFIG_AES_IV_WORD3=\).*/\10x'`echo $UBOOT_IV_KEY | cut -c 25-32`/ $1/.config || exit 12
sed -i 's/\(^CONFIG_AES_CMAC_KEY_WORD0=\).*/\10x'`echo $UBOOT_CMAC_KEY | cut -c 1-8`/   $1/.config || exit 13
sed -i 's/\(^CONFIG_AES_CMAC_KEY_WORD1=\).*/\10x'`echo $UBOOT_CMAC_KEY | cut -c 9-16`/  $1/.config || exit 14
sed -i 's/\(^CONFIG_AES_CMAC_KEY_WORD2=\).*/\10x'`echo $UBOOT_CMAC_KEY | cut -c 17-24`/ $1/.config || exit 15
sed -i 's/\(^CONFIG_AES_CMAC_KEY_WORD3=\).*/\10x'`echo $UBOOT_CMAC_KEY | cut -c 25-32`/ $1/.config || exit 16
sed -i 's/\(^CONFIG_AES_CMAC_KEY_WORD4=\).*/\10x'`echo $UBOOT_CMAC_KEY | cut -c 33-40`/ $1/.config || exit 17
sed -i 's/\(^CONFIG_AES_CMAC_KEY_WORD5=\).*/\10x'`echo $UBOOT_CMAC_KEY | cut -c 41-48`/ $1/.config || exit 18
sed -i 's/\(^CONFIG_AES_CMAC_KEY_WORD6=\).*/\10x'`echo $UBOOT_CMAC_KEY | cut -c 49-56`/ $1/.config || exit 19
sed -i 's/\(^CONFIG_AES_CMAC_KEY_WORD7=\).*/\10x'`echo $UBOOT_CMAC_KEY | cut -c 57-64`/ $1/.config || exit 20

#
# Create app_key.h. The file will look like:
#   #define APP_KEY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
#
APP_KEY_FILENAME=app.key
until pkcs11-tool --module opensc-pkcs11.so --login --read-object --type data --label app_key --output-file $1/$APP_KEY_FILENAME; do
    echo -n "pkcs11-tool failed. Did you connect the NitroKey HSM dongle? Press Enter to retry."
    read null
done

APP_KEY_HEADER_FILE=$1/contrib/board/dots_clare/app_key.h
cd $1
xxd --include $APP_KEY_FILENAME > $APP_KEY_HEADER_FILE
rm -f $1/$APP_KEY_FILENAME
