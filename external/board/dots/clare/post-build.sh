#!/bin/sh

# Copy locale program from toolchain to target. Eclipse Remote Debugging with SSH needs locale on the target.
cp $STAGING_DIR/usr/bin/locale $TARGET_DIR/usr/bin || exit 1

# Copy version file to target
cp ../version $TARGET_DIR/etc || exit 2

# Add commit id to version
echo -n "GIT_COMMIT_ID=" >> $TARGET_DIR/etc/version || exit 3
git log --pretty=format:%h -n 1 >> $TARGET_DIR/etc/version || exit 4

# Add indication if git is dirty, i.e. if there's change that's not checked in. "D" is for dirty.
if ! git diff-index --quiet HEAD --; then
    echo -n "_dirty" >> $TARGET_DIR/etc/version || exit 5
fi

# Add public key to check signature of FIT images
dtc -O dtb $BR2_EXTERNAL_MCHP_PATH/board/dots/clare/public_key.dts > $TARGET_DIR/etc/public_key.dtb || exit 6

# Add device tree overlays
cp $BINARIES_DIR/*overlay.dtb $TARGET_DIR/etc/ || exit 7
