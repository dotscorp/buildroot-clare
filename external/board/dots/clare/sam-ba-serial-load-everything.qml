import SAMBA 3.2
import SAMBA.Connection.Serial 3.2
import SAMBA.Device.SAMA5D2 3.2

SerialConnection {
	port: "ttyACM0"

	device: SAMA5D2 {
		config {
			nandflash {
				ioset: 2
				busWidth: 8
				header: 0xc0c00405
			}
		}
	}

	onConnectionOpened: {
		// initialize NAND flash applet
		initializeApplet("nandflash")

		// erase all memory
		applet.erase(0, applet.memorySize)

		// write files
		applet.write(0x00000, "boot.bin", true)
		applet.write(0x20000, "u-boot.cip")
		applet.write(0xA0000, "dots.ubi")

		// Reboot. Don't use boot configuration from bureg.
		writeu32(0xF8048054,0x66830000)
		print("Resetting...")
		writeu32(0xF8048000,0xA5000001)
	}
}
