/*
 * Omnivision OV9281 CMOS Image Sensor driver
 *
 * Based on OV2659 driver
 *
 * Nhat Nam Trinh <ntrinh@dotscorp.com>
 *
 * This program is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/i2c.h>
#include <linux/kernel.h>
#include <linux/media.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_graph.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/videodev2.h>

#include <media/media-entity.h>
#include <media/v4l2-common.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-device.h>
#include <media/v4l2-event.h>
#include <media/v4l2-fwnode.h>
#include <media/v4l2-image-sizes.h>
#include <media/v4l2-mediabus.h>
#include <media/v4l2-subdev.h>

#define DRIVER_NAME "ov9281"

#define V4L2_CID_TARGET_BLACK_LEVEL (V4L2_CID_USER_BASE | 0x1002)

/*
 * OV9281 register definitions
 */
#define REG_SC_MODE_SELECT	    0x0100
#define REG_SC_SOFTWARE_RESET	0x0103
#define REG_SC_CHIP_ID_H		0x300a
#define REG_SC_CHIP_ID_L		0x300b
#define REG_AEC_EXPO_0			0x3500
#define REG_AEC_EXPO_1			0x3501
#define REG_AEC_EXPO_2			0x3502
#define REG_GAIN				0x3509

#define REG_GENARAL_TEST_PATTERN	0x5e00
#define GENERAL_TEST_PATTERN_ENABLE	BIT(7)

#define REG_NULL			0x0000	/* Array end token */

#define OV_ID(_msb, _lsb)		((_msb) << 8 | (_lsb))
#define OV9281_ID			0x9281

#define DEFAULT_EXPOSURE_100US	2000	/* 100-microsecond unit */
#define DEFAULT_GAIN		8
#define DEFAULT_TARGET_BLACK_LEVEL	0

struct sensor_register {
	u16 addr;
	u8 value;
};

struct ov9281_framesize {
	u16 width;
	u16 height;
	const struct sensor_register *regs;
};

struct ov9281_pixfmt {
	u32 code;
};

struct pll_ctrl_reg {
	unsigned int div;
	unsigned char reg;
};

struct ov9281 {
	struct v4l2_subdev sd;
	struct media_pad pad;
	struct v4l2_mbus_framefmt format;
	unsigned int xvclk_frequency;
	struct mutex lock;
	struct i2c_client *client;
	struct v4l2_ctrl_handler ctrls;
	const struct ov9281_framesize *frame_size;
	int streaming;
};

static int ov9281_s_ctrl(struct v4l2_ctrl *ctrl);

static const struct sensor_register ov9281_init_regs[] = {
#if 0
	/* Clocks, assuming xvclk is 24MHz. PLL1 pix clk = 48MHz, PLL2 analog clk = 48MHz, PLL2 sys clk = 24MHz. */
	{ 0x030a,  0 },
	{ 0x0300,  1 },
	{ 0x0301,  0  },
	{ 0x0302, 48 },
	{ 0x0303,  1 },
	{ 0x0304,  3 },
	{ 0x0305,  2 },
	{ 0x0306,  1 },
	{ 0x0314,  0 },
	{ 0x030b,  4 },
	{ 0x030c,  0 },
    { 0x030d, 96 },
    { 0x0313,  1 },
    { 0x0312, 15 },
	{ 0x030f, 15 },
	{ 0x030e, 2 },
#else
	/* Clocks, assuming xvclk is 24MHz. PLL1 pix clk = 96MHz, PLL2 analog clk = 96MHz, PLL2 sys clk = 48MHz. */
	{ 0x030a,  0 },
	{ 0x0300,  1 },
	{ 0x0301,  0  },
	{ 0x0302, 48 },
	{ 0x0303,  0 },
	{ 0x0304,  3 },
	{ 0x0305,  2 },
	{ 0x0306,  1 },
	{ 0x0314,  0 },
	{ 0x030b,  4 },
	{ 0x030c,  0 },
	{ 0x030d, 96 },
	{ 0x0313,  1 },
	{ 0x0312,  7 },
	{ 0x030f,  3 },
	{ 0x030e,  6 },
#endif

	{ 0x3001, 0x62 },
	{ 0x3004, 0x01 },
	{ 0x3005, 0xff },
	{ 0x3006, 0xe2 },
	{ 0x3011, 0x0a },
	{ 0x3013, 0x18 },
	{ 0x301c, 0xf0 },
	{ 0x3022, 0x07 },
	{ 0x3030, 0x10 },
	{ 0x3039, 0x2e },
	{ 0x303a, 0xf0 },
	{ 0x3500, 0x00 },
	{ 0x3501, 0x2a },
	{ 0x3502, 0x90 },
	{ 0x3503, 0x08 },
	{ 0x3505, 0x8c },
	{ 0x3507, 0x03 },
	{ 0x3508, 0x00 },
	{ 0x3509, 0x10 },
	{ 0x3610, 0x80 },
	{ 0x3611, 0xa0 },
	{ 0x3620, 0x6e },
	{ 0x3632, 0x56 },
	{ 0x3633, 0x78 },
	{ 0x3662, 0x05 },
	{ 0x3666, 0x5a },
	{ 0x366f, 0x7e },
	{ 0x3680, 0x84 },
	{ 0x3712, 0x80 },
	{ 0x372d, 0x22 },
	{ 0x3731, 0x80 },
	{ 0x3732, 0x30 },
	{ 0x3778, 0x00 },
	{ 0x377d, 0x22 },
	{ 0x3788, 0x02 },
	{ 0x3789, 0xa4 },
	{ 0x378a, 0x00 },
	{ 0x378b, 0x4a },
	{ 0x3799, 0x20 },
	{ 0x3800, 0x00 },
	{ 0x3801, 0x00 },
	{ 0x3802, 0x00 },
	{ 0x3803, 0x00 },
	{ 0x3804, 0x05 },
	{ 0x3805, 0x0f },
	{ 0x3806, 0x03 },
	{ 0x3807, 0x2f },
	{ 0x3808, 0x05 },
	{ 0x3809, 0x00 },
	{ 0x380a, 0x03 },
	{ 0x380b, 0x20 },
	{ 0x380c, 0x02 },
	{ 0x380d, 0xd8 },
	{ 0x380e, 0x03 },
	{ 0x380f, 0x8e },
	{ 0x3810, 0x00 },
	{ 0x3811, 0x08 },
	{ 0x3812, 0x00 },
	{ 0x3813, 0x08 },
	{ 0x3814, 0x11 },
	{ 0x3815, 0x11 },
	{ 0x3820, 0x40 },
	{ 0x3821, 0x00 },
	{ 0x382c, 0x05 },
	{ 0x382d, 0xb0 },
	{ 0x389d, 0x00 },
	{ 0x3881, 0x42 },
	{ 0x3882, 0x01 },
	{ 0x3883, 0x00 },
	{ 0x3885, 0x02 },
	{ 0x38a8, 0x02 },
	{ 0x38a9, 0x80 },
	{ 0x38b1, 0x00 },
	{ 0x38b3, 0x02 },
	{ 0x38c4, 0x00 },
	{ 0x38c5, 0xc0 },
	{ 0x38c6, 0x04 },
	{ 0x38c7, 0x80 },
	{ 0x3920, 0xff },
	{ 0x4003, 0x40 },
	{ 0x4008, 0x04 },
	{ 0x4009, 0x0b },
	{ 0x400c, 0x00 },
	{ 0x400d, 0x07 },
	{ 0x4010, 0x40 },
	{ 0x4043, 0x40 },
	{ 0x4307, 0x30 },
	{ 0x4317, 0x01 },
	{ 0x4501, 0x00 },
	{ 0x4507, 0x00 },
	{ 0x4509, 0x00 },
	{ 0x450a, 0x08 },
	{ 0x4601, 0x04 },
	{ 0x470f, 0xe0 },
	{ 0x4f07, 0x00 },
	{ 0x4800, 0x00 },
	{ 0x5000, 0x9f },
	{ 0x5001, 0x00 },
	{ 0x5e00, 0x00 },
	{ 0x5d00, 0x0b },
	{ 0x5d01, 0x02 },
	{ 0x4f00, 0x04 },
	{ 0x4f10, 0x00 },
	{ 0x4f11, 0x98 },
	{ 0x4f12, 0x0f },
	{ 0x4f13, 0xc4 },
	{ REG_NULL, 0x00 },
};

static const struct v4l2_ctrl_ops ov9281_ctrl_ops = {
	.s_ctrl = ov9281_s_ctrl,
};

static const char * const ov9281_test_pattern_menu[] = {
	"Disabled",
	"General",
};

static const struct v4l2_ctrl_config ov9281_ctrls[] = {
	{
		.ops		= &ov9281_ctrl_ops,
		.id			= V4L2_CID_TARGET_BLACK_LEVEL,
		.type		= V4L2_CTRL_TYPE_INTEGER,
		.name		= "Target Black Level",
		.min		= 0,
		.max		= 1023,
		.step		= 1,
		.def		= DEFAULT_TARGET_BLACK_LEVEL,
		.flags		= 0,
	}
};

/* 1280X800 wxga */
static struct sensor_register ov9281_wxga[] = {
	{ REG_NULL, 0x00 },
};

static const struct ov9281_framesize ov9281_framesizes[] = {
    { /* WXGA */
		.width		= 1280,
		.height		= 800,
		.regs		= ov9281_wxga,
	},
};

static const struct ov9281_pixfmt ov9281_formats[] = {
	{
		.code = MEDIA_BUS_FMT_Y8_1X8
	}
};

static inline struct ov9281 *to_ov9281(struct v4l2_subdev *sd)
{
	return container_of(sd, struct ov9281, sd);
}

/* sensor register write */
static int ov9281_write(struct i2c_client *client, u16 reg, u8 val)
{
	struct i2c_msg msg;
	u8 buf[3];
	int ret;

	buf[0] = reg >> 8;
	buf[1] = reg & 0xFF;
	buf[2] = val;

	msg.addr = client->addr;
	msg.flags = client->flags;
	msg.buf = buf;
	msg.len = sizeof(buf);

	ret = i2c_transfer(client->adapter, &msg, 1);
	if (ret >= 0)
		return 0;

	dev_dbg(&client->dev,
		"ov9281 write reg(0x%x val:0x%x) failed !\n", reg, val);

	return ret;
}

/* sensor register read */
static int ov9281_read(struct i2c_client *client, u16 reg, u8 *val)
{
	struct i2c_msg msg[2];
	u8 buf[2];
	int ret;

	buf[0] = reg >> 8;
	buf[1] = reg & 0xFF;

	msg[0].addr = client->addr;
	msg[0].flags = client->flags;
	msg[0].buf = buf;
	msg[0].len = sizeof(buf);

	msg[1].addr = client->addr;
	msg[1].flags = client->flags | I2C_M_RD;
	msg[1].buf = buf;
	msg[1].len = 1;

	ret = i2c_transfer(client->adapter, msg, 2);
	if (ret >= 0) {
		*val = buf[0];
		return 0;
	}

	dev_dbg(&client->dev,
		"ov9281 read reg(0x%x val:0x%x) failed !\n", reg, *val);

	return ret;
}

static int ov9281_write_array(struct i2c_client *client,
			      const struct sensor_register *regs)
{
	int i, ret = 0;

	for (i = 0; ret == 0 && regs[i].addr; i++)
		ret = ov9281_write(client, regs[i].addr, regs[i].value);

	return ret;
}

static void ov9281_get_default_format(struct v4l2_mbus_framefmt *format)
{
	format->width = ov9281_framesizes[0].width;
	format->height = ov9281_framesizes[0].height;
	format->colorspace = V4L2_COLORSPACE_SRGB;
	format->code = ov9281_formats[0].code;
	format->field = V4L2_FIELD_NONE;
}

static void ov9281_set_streaming(struct ov9281 *ov9281, int on)
{
	struct i2c_client *client = ov9281->client;
	int ret;

	on = !!on;

	dev_dbg(&client->dev, "%s: on: %d\n", __func__, on);

	ret = ov9281_write(client, REG_SC_MODE_SELECT, on);
	if (ret)
		dev_err(&client->dev, "ov9281 soft standby failed\n");
}

static int ov9281_init(struct v4l2_subdev *sd, u32 val)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);

	return ov9281_write_array(client, ov9281_init_regs);
}

/*
 * V4L2 subdev video and pad level operations
 */

static int ov9281_enum_mbus_code(struct v4l2_subdev *sd,
				 struct v4l2_subdev_pad_config *cfg,
				 struct v4l2_subdev_mbus_code_enum *code)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);

	dev_dbg(&client->dev, "%s:\n", __func__);

	if (code->index >= ARRAY_SIZE(ov9281_formats))
		return -EINVAL;

	code->code = ov9281_formats[code->index].code;

	return 0;
}

static int ov9281_enum_frame_sizes(struct v4l2_subdev *sd,
				   struct v4l2_subdev_pad_config *cfg,
				   struct v4l2_subdev_frame_size_enum *fse)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	int i = ARRAY_SIZE(ov9281_formats);

	dev_dbg(&client->dev, "%s:\n", __func__);

	if (fse->index >= ARRAY_SIZE(ov9281_framesizes))
		return -EINVAL;

	while (--i)
		if (fse->code == ov9281_formats[i].code)
			break;

	fse->code = ov9281_formats[i].code;

	fse->min_width  = ov9281_framesizes[fse->index].width;
	fse->max_width  = fse->min_width;
	fse->max_height = ov9281_framesizes[fse->index].height;
	fse->min_height = fse->max_height;

	return 0;
}

static int ov9281_get_fmt(struct v4l2_subdev *sd,
			  struct v4l2_subdev_pad_config *cfg,
			  struct v4l2_subdev_format *fmt)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	struct ov9281 *ov9281 = to_ov9281(sd);

	dev_dbg(&client->dev, "ov9281_get_fmt\n");

	if (fmt->which == V4L2_SUBDEV_FORMAT_TRY) {
#ifdef CONFIG_VIDEO_V4L2_SUBDEV_API
		struct v4l2_mbus_framefmt *mf;

		mf = v4l2_subdev_get_try_format(sd, cfg, 0);
		mutex_lock(&ov9281->lock);
		fmt->format = *mf;
		mutex_unlock(&ov9281->lock);
		return 0;
#else
	return -ENOTTY;
#endif
	}

	mutex_lock(&ov9281->lock);
	fmt->format = ov9281->format;
	mutex_unlock(&ov9281->lock);

	dev_dbg(&client->dev, "ov9281_get_fmt: %x %dx%d\n",
		ov9281->format.code, ov9281->format.width,
		ov9281->format.height);

	return 0;
}

static void __ov9281_try_frame_size(struct v4l2_mbus_framefmt *mf,
				    const struct ov9281_framesize **size)
{
	const struct ov9281_framesize *fsize = &ov9281_framesizes[0];
	const struct ov9281_framesize *match = NULL;
	int i = ARRAY_SIZE(ov9281_framesizes);
	unsigned int min_err = UINT_MAX;

	while (i--) {
		int err = abs(fsize->width - mf->width)
				+ abs(fsize->height - mf->height);
		if ((err < min_err) && (fsize->regs[0].addr)) {
			min_err = err;
			match = fsize;
		}
		fsize++;
	}

	if (!match)
		match = &ov9281_framesizes[0];

	mf->width  = match->width;
	mf->height = match->height;

	if (size)
		*size = match;
}

static int ov9281_set_fmt(struct v4l2_subdev *sd,
			  struct v4l2_subdev_pad_config *cfg,
			  struct v4l2_subdev_format *fmt)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	int index = ARRAY_SIZE(ov9281_formats);
	struct v4l2_mbus_framefmt *mf = &fmt->format;
	const struct ov9281_framesize *size = NULL;
	struct ov9281 *ov9281 = to_ov9281(sd);
	int ret = 0;

	dev_dbg(&client->dev, "ov9281_set_fmt\n");

	__ov9281_try_frame_size(mf, &size);

	while (--index >= 0)
		if (ov9281_formats[index].code == mf->code)
			break;

	if (index < 0) {
		index = 0;
		mf->code = ov9281_formats[index].code;
	}

	mf->colorspace = V4L2_COLORSPACE_SRGB;
	mf->field = V4L2_FIELD_NONE;

	mutex_lock(&ov9281->lock);

	if (fmt->which == V4L2_SUBDEV_FORMAT_TRY) {
#ifdef CONFIG_VIDEO_V4L2_SUBDEV_API
		mf = v4l2_subdev_get_try_format(sd, cfg, fmt->pad);
		*mf = fmt->format;
#else
		return -ENOTTY;
#endif
	} else {
		if (ov9281->streaming) {
			mutex_unlock(&ov9281->lock);
			return -EBUSY;
		}

		ov9281->frame_size = size;
		ov9281->format = fmt->format;
	}

	mutex_unlock(&ov9281->lock);
	return ret;
}

static int ov9281_set_frame_size(struct ov9281 *ov9281)
{
	struct i2c_client *client = ov9281->client;

	dev_dbg(&client->dev, "%s\n", __func__);

	return ov9281_write_array(ov9281->client, ov9281->frame_size->regs);
}

static int ov9281_s_stream(struct v4l2_subdev *sd, int on)
{
	struct ov9281 *ov9281 = to_ov9281(sd);
	int ret = 0;

	mutex_lock(&ov9281->lock);

	on = !!on;

	if (ov9281->streaming == on)
		goto unlock;

	if (!on) {
		/* Stop Streaming Sequence */
		ov9281_set_streaming(ov9281, 0);
		ov9281->streaming = on;
		goto unlock;
	}

	ov9281_set_frame_size(ov9281);
	ov9281_set_streaming(ov9281, 1);
	ov9281->streaming = on;

unlock:
	mutex_unlock(&ov9281->lock);
	return ret;
}

static int ov9281_set_test_pattern(struct ov9281 *ov9281, int value)
{
	struct i2c_client *client = v4l2_get_subdevdata(&ov9281->sd);
	int ret;
	u8 val;

	ret = ov9281_read(client, REG_GENARAL_TEST_PATTERN, &val);
	if (ret < 0)
		return ret;

	switch (value) {
	case 0:
		val &= ~GENERAL_TEST_PATTERN_ENABLE;
		break;
	case 1:
		val |= GENERAL_TEST_PATTERN_ENABLE;
		break;
	}

	return ov9281_write(client, REG_GENARAL_TEST_PATTERN, val);
}

static int ov9281_set_exposure(struct ov9281 *ov9281, s32 value /* 100-microsecond unit */)
{
	struct i2c_client *client = v4l2_get_subdevdata(&ov9281->sd);
	u8 pll2_pre_div1_x2, pll2_sys_div_x2;
	u8 pll2_pre_div0, r30b, r30c, r30d, r30f, r30e, r3814, r380c, r380d, r380e, r380f, pll2_sys_prediv, hts_ratio;
	u16 pll2_mul, expt_100us;
	u32 sclk_khz, hts, expl, vts;
	struct sensor_register exp_regs[] = {
		{ REG_AEC_EXPO_0, 0 },
		{ REG_AEC_EXPO_1, 0 },
		{ REG_AEC_EXPO_2, 0 },
		{ REG_NULL, 0 }
	};
	
	
	ov9281_read(client, 0x0314, &pll2_pre_div0);
	pll2_pre_div0 = (pll2_pre_div0 & 0x01) + 1;
	ov9281_read(client, 0x030b, &r30b);
	r30b &= 0x07;
	
	if (r30b < 5) {
		pll2_pre_div1_x2 = r30b + 2;
	}
	else {
		pll2_pre_div1_x2 = r30b * 4 - 12;
	}
	
	ov9281_read(client, 0x030c, &r30c);
	ov9281_read(client, 0x030d, &r30d);
	pll2_mul = (r30c & 0x03) * 256 + r30d;
	
	ov9281_read(client, 0x030f, &r30f);
	pll2_sys_prediv = (r30f & 0x0f) + 1;
	
	ov9281_read(client, 0x030e, &r30e);
	r30e &= 0x07;
	pll2_sys_div_x2 = r30e + 2;
	if (pll2_sys_div_x2 == 9) {
		pll2_sys_div_x2 = 10;
	}
	
	sclk_khz = ((ov9281->xvclk_frequency/1000) * pll2_mul * (2 * 2)) / (pll2_pre_div0 * pll2_pre_div1_x2 * pll2_sys_prediv * pll2_sys_div_x2);
		
	expt_100us = value;
	ov9281_read(client, 0x3814, &r3814);
	hts_ratio = r3814 / 16 + r3814 % 16;
	ov9281_read(client, 0x380c, &r380c);
	ov9281_read(client, 0x380d, &r380d);
	hts = (r380c * 256 + r380d) * 4 / hts_ratio;
	if (hts_ratio == 2) {
		sclk_khz = sclk_khz * 2;
	}
		
	expl = expt_100us*sclk_khz/(hts*10);
	
	ov9281_read(client, 0x380e, &r380e);
	ov9281_read(client, 0x380f, &r380f);
	vts = r380e * 256 + r380f;
	
	if (expl > vts - 6) {
	   vts = (expl + 6) & 0xffff;
	   ov9281_write(v4l2_get_subdevdata(&ov9281->sd), 0x380e, vts / 256);
	   ov9281_write(v4l2_get_subdevdata(&ov9281->sd), 0x380f, vts % 256);
	}
	else if (expl < 0) {
		dev_err(&client->dev, "Invalid Exposure\n");
	    return 1; 
	}

	exp_regs[0].value = (expl & 0xf000) >> 12;
	exp_regs[1].value = (expl & 0x0ff0) >> 4;
	exp_regs[2].value = (expl & 0x000f) << 4;
	
	return ov9281_write_array(v4l2_get_subdevdata(&ov9281->sd), &exp_regs[0]);
}

static int ov9281_set_gain(struct ov9281 *ov9281, s32 gain)
{	
	return ov9281_write(v4l2_get_subdevdata(&ov9281->sd), REG_GAIN, (u8)(gain * 16));
}

static int ov9281_set_target_black_level(struct ov9281 *ov9281, s32 target_black_level)
{
	struct sensor_register regs[] = {
		{ 0x4002, 0 },
		{ 0x4003, 0 },
		{ REG_NULL, 0 }
	};

	regs[0].value = ((u32)target_black_level & 0xff00) >> 8;
	regs[1].value = (u32)target_black_level & 0xff;
	
	return ov9281_write_array(v4l2_get_subdevdata(&ov9281->sd), &regs[0]);
}

static int ov9281_s_ctrl(struct v4l2_ctrl *ctrl)
{
	struct ov9281 *ov9281 =
			container_of(ctrl->handler, struct ov9281, ctrls);

	switch (ctrl->id) {
	case V4L2_CID_TEST_PATTERN:
		return ov9281_set_test_pattern(ov9281, ctrl->val);
	case V4L2_CID_GAIN:
		return ov9281_set_gain(ov9281, ctrl->val);
	case V4L2_CID_EXPOSURE_ABSOLUTE:
		return ov9281_set_exposure(ov9281, ctrl->val);
	case V4L2_CID_TARGET_BLACK_LEVEL:
		return ov9281_set_target_black_level(ov9281, ctrl->val);
	}

	return 0;
}

/* -----------------------------------------------------------------------------
 * V4L2 subdev internal operations
 */
static const struct v4l2_subdev_core_ops ov9281_subdev_core_ops = {
	.log_status = v4l2_ctrl_subdev_log_status,
	.subscribe_event = v4l2_ctrl_subdev_subscribe_event,
	.unsubscribe_event = v4l2_event_subdev_unsubscribe,
};

static const struct v4l2_subdev_video_ops ov9281_subdev_video_ops = {
	.s_stream = ov9281_s_stream,
};

static const struct v4l2_subdev_pad_ops ov9281_subdev_pad_ops = {
	.enum_mbus_code = ov9281_enum_mbus_code,
	.enum_frame_size = ov9281_enum_frame_sizes,
	.get_fmt = ov9281_get_fmt,
	.set_fmt = ov9281_set_fmt,
};

#ifdef CONFIG_VIDEO_V4L2_SUBDEV_API
static const struct v4l2_subdev_ops ov9281_subdev_ops = {
	.core  = &ov9281_subdev_core_ops,
	.video = &ov9281_subdev_video_ops,
	.pad   = &ov9281_subdev_pad_ops,
};
#endif

static int ov9281_detect(struct v4l2_subdev *sd)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	u8 pid = 0;
	u8 ver = 0;
	int ret;

	dev_dbg(&client->dev, "%s:\n", __func__);

	ret = ov9281_write(client, REG_SC_SOFTWARE_RESET, 0x01);
	if (ret != 0) {
		dev_err(&client->dev, "Sensor soft reset failed\n");
		return -ENODEV;
	}
	usleep_range(1000, 2000);

	/* Check sensor revision */
	ret = ov9281_read(client, REG_SC_CHIP_ID_H, &pid);
	if (!ret)
		ret = ov9281_read(client, REG_SC_CHIP_ID_L, &ver);

	if (!ret) {
		unsigned short id;

		id = OV_ID(pid, ver);
		if (id != OV9281_ID)
			dev_err(&client->dev,
				"Sensor detection failed (%04X, %d)\n",
				id, ret);
		else {
			dev_info(&client->dev, "Found OV%04X sensor\n", id);
			ret = ov9281_init(sd, 0);
		}
	}

	return ret;
}

static int ov9281_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	struct v4l2_subdev *sd;
	struct ov9281 *ov9281;
	struct clk *clk;
	int idx, ret;

	ov9281 = devm_kzalloc(&client->dev, sizeof(*ov9281), GFP_KERNEL);
	if (!ov9281)
		return -ENOMEM;

	ov9281->client = client;

	clk = devm_clk_get(&client->dev, "xvclk");
	if (IS_ERR(clk))
		return PTR_ERR(clk);

	ov9281->xvclk_frequency = clk_get_rate(clk);
    /* Make sure xvclk_frequency is 24MHz +/- 10% */
	if (ov9281->xvclk_frequency < 24000000 * 9 / 10 && ov9281->xvclk_frequency > 24000000 * 11 / 10) {
        dev_err(&client->dev, "can only handle xvclk frequency of 24000000");
		return -EINVAL;
    }

    /* Enable xvclk */
    clk_prepare_enable(clk);

	v4l2_ctrl_handler_init(&ov9281->ctrls, ARRAY_SIZE(ov9281_ctrls) + 4);
	
	v4l2_ctrl_new_std(&ov9281->ctrls, &ov9281_ctrl_ops,
					V4L2_CID_EXPOSURE_ABSOLUTE, 10, 10000, 1, DEFAULT_EXPOSURE_100US);
	v4l2_ctrl_new_std(&ov9281->ctrls, &ov9281_ctrl_ops,
					 V4L2_CID_GAIN, 1, 15, 1, DEFAULT_GAIN);
	v4l2_ctrl_new_std_menu_items(&ov9281->ctrls, &ov9281_ctrl_ops,
				     V4L2_CID_TEST_PATTERN,
				     ARRAY_SIZE(ov9281_test_pattern_menu) - 1,
				     0, 0, ov9281_test_pattern_menu);
	for (idx = 0; idx < ARRAY_SIZE(ov9281_ctrls); ++idx)
		v4l2_ctrl_new_custom(&ov9281->ctrls, &ov9281_ctrls[idx], NULL);

	ov9281->sd.ctrl_handler = &ov9281->ctrls;

	if (ov9281->ctrls.error) {
		dev_err(&client->dev, "%s: control initialization error %d\n",
			__func__, ov9281->ctrls.error);
		return  ov9281->ctrls.error;
	}

	sd = &ov9281->sd;
	client->flags |= I2C_CLIENT_SCCB;
#ifdef CONFIG_VIDEO_V4L2_SUBDEV_API
	v4l2_i2c_subdev_init(sd, client, &ov9281_subdev_ops);
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE |
		     V4L2_SUBDEV_FL_HAS_EVENTS;
#endif

#if defined(CONFIG_MEDIA_CONTROLLER)
	ov9281->pad.flags = MEDIA_PAD_FL_SOURCE;
	sd->entity.function = MEDIA_ENT_F_CAM_SENSOR;
	ret = media_entity_pads_init(&sd->entity, 1, &ov9281->pad);
	if (ret < 0) {
		v4l2_ctrl_handler_free(&ov9281->ctrls);
		return ret;
	}
#endif

	mutex_init(&ov9281->lock);

	ov9281_get_default_format(&ov9281->format);
	ov9281->frame_size = &ov9281_framesizes[0];

	ret = ov9281_detect(sd);
	if (ret < 0)
		goto error;

	ret = v4l2_async_register_subdev(&ov9281->sd);
	if (ret)
		goto error;
		
	ov9281_set_exposure(ov9281, DEFAULT_EXPOSURE_100US);
	ov9281_set_gain(ov9281, DEFAULT_GAIN);
	ov9281_set_target_black_level(ov9281, DEFAULT_TARGET_BLACK_LEVEL);

	dev_info(&client->dev, "%s sensor driver registered !!\n", sd->name);

	return 0;

error:
	v4l2_ctrl_handler_free(&ov9281->ctrls);
	media_entity_cleanup(&sd->entity);
	mutex_destroy(&ov9281->lock);
	return ret;
}

static int ov9281_remove(struct i2c_client *client)
{
	struct v4l2_subdev *sd = i2c_get_clientdata(client);
	struct ov9281 *ov9281 = to_ov9281(sd);

	v4l2_ctrl_handler_free(&ov9281->ctrls);
	v4l2_async_unregister_subdev(sd);
	media_entity_cleanup(&sd->entity);
	mutex_destroy(&ov9281->lock);

	return 0;
}

static const struct i2c_device_id ov9281_id[] = {
	{ "ov9281", 0 },
	{ /* sentinel */ },
};
MODULE_DEVICE_TABLE(i2c, ov9281_id);

#if IS_ENABLED(CONFIG_OF)
static const struct of_device_id ov9281_of_match[] = {
	{ .compatible = "ovti,ov9281", },
	{ /* sentinel */ },
};
MODULE_DEVICE_TABLE(of, ov9281_of_match);
#endif

static struct i2c_driver ov9281_i2c_driver = {
	.driver = {
		.name	= DRIVER_NAME,
		.of_match_table = of_match_ptr(ov9281_of_match),
	},
	.probe		= ov9281_probe,
	.remove		= ov9281_remove,
	.id_table	= ov9281_id,
};

module_i2c_driver(ov9281_i2c_driver);

MODULE_AUTHOR("Nhat Nam Trinh <ntrinh@dotscorp.com>");
MODULE_DESCRIPTION("OV9281 CMOS Image Sensor driver");
MODULE_LICENSE("GPL v2");
