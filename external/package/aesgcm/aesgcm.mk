################################################################################
#
# aesgcm
#
################################################################################
AESGCM_VERSION = 28310a1aa06b2184578b339e2c4431e6f47caee7
AESGCM_SITE = $(call github,jforissier,aesgcm,$(AESGCM_VERSION))
AESGCM_LICENSE = BSD-2-Clause
AESGCM_DEPENDENCIES = openssl

define AESGCM_BUILD_CMDS
    $(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D)
endef

define AESGCM_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/aesgcm $(TARGET_DIR)/usr/bin
endef


$(eval $(generic-package))